data "aws_caller_identity" "current" {}

data "aws_iam_role" "lab_role" {
  name = "LabRole"
}

data "aws_iam_instance_profile" "lab_instance_profile" {
  name = "LabInstanceProfile"
}

data "aws_iam_policy_document" "lab_instance_profile" {
  // Configuration for retrieving the policy document
  // ...
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name               = "ecs_task_execution_role"
  assume_role_policy = data.aws_iam_policy_document.lab_instance_profile.json
}

data "aws_iam_policy" "ecs_task_execution_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy" "ecs_task_execution_policy" {
  name   = "ecs_task_execution_policy"
  role   = aws_iam_role.ecs_task_execution_role.id
  policy = data.aws_iam_policy.ecs_task_execution_policy.policy
}

data "aws_iam_policy_document" "instance_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy" "ecs_instance_role_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_role" "ecs_instance_role" {
  name               = "ecs_instance_role"
  assume_role_policy = data.aws_iam_policy_document.instance_assume_role_policy.json
}

resource "aws_iam_role_policy" "ecs_instance_role_policy" {
  name   = "ecs_instance_role_policy"
  role   = aws_iam_role.ecs_instance_role.id
  policy = data.aws_iam_policy.ecs_instance_role_policy.policy
}

resource "aws_iam_instance_profile" "ecs_instance_role" {
  name = "ecs_instance_profile_role"
  role = aws_iam_role.ecs_instance_role.name
}
resource "aws_iam_policy" "ecs_instance_role_creation_policy" {
  name        = "ecs_instance_role_creation_policy"
  description = "Allows the creation of ECS instance roles."

  policy = jsonencode({
    Version: "2012-10-17",
    Statement: [{
      Effect: "Allow",
      Action: "iam:CreateRole",
      Resource: "arn:aws:iam::423214585559:role/ecs_instance_role"
    }]
  })
}

